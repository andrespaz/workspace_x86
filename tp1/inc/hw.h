#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>
#include <time.h>

/*==================[macros]=================================================*/

#define EXIT      27  // ASCII para la tecla Esc 
#define SENSOR_1  49  // ASCII para la tecla 1 
#define SENSOR_2  50  // ASCII para la tecla 2

#define ESPERA_VEHICULO_CIRCULANDO 5000
#define CUENTA_VIGENTE  10 // Código de interrupción inventado
#define TEMP_REINCIADO 20   // Código de interrupción inventado
#define TOCA_CERRAR 30   // Código de interrupción inventado

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

void hw_Init(void);
void hw_DeInit(void);
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);
void hw_AbrirBarrera(void);
void hw_CerrarBarrera(void);
void hw_accionarAlarma(void);
void hw_darTiempoCierre(void);
void hw_flushTemporizador(void);

uint8_t hw_pseudoInterrupts(void);
void hw_flushInterrupts(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
