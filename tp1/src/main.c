/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;
    uint8_t falsaInterrupcion;

    uint8_t auto_circulando=0;
    uint8_t barrera_abierta=0;

    hw_Init();

    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT) {
        input = hw_LeerEntrada();
        falsaInterrupcion = hw_pseudoInterrupts();

        if (input == SENSOR_1) {
            hw_AbrirBarrera();
            auto_circulando=1;
            if(barrera_abierta==1){
                hw_flushTemporizador();
            }
            else{
                barrera_abierta=1;
            }
        }

        if (input == SENSOR_2) {
            if(barrera_abierta==0){
                hw_accionarAlarma();
            }
            else{
                auto_circulando=0;
                hw_darTiempoCierre(); 
           }
        }


        if(falsaInterrupcion == TOCA_CERRAR){
            hw_flushInterrupts();
            if(auto_circulando==0){
                hw_CerrarBarrera();
                barrera_abierta=0;
            }
        }

        hw_Pausems(100);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
