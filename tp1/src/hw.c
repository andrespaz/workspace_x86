/*==================[inclusions]=============================================*/

#include "hw.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;

uint8_t codigoInterrupt;
time_t inicioCuenta; 

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);       
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);    
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}



void hw_AbrirBarrera(void)
{
    printf("Vehiculo ingresando \n\n");
}

void hw_CerrarBarrera(void)
{
    printf("Se cierra la barrera\n\n");
}

void hw_accionarAlarma(void)
{
    printf("Suena el buzzer-El auto no debe estar aca \n\n");
}

void hw_darTiempoCierre(void)
{
    inicioCuenta = time(NULL); 
    codigoInterrupt=CUENTA_VIGENTE; 
}

void hw_flushTemporizador(void)
{
    codigoInterrupt=TEMP_REINCIADO;
}

uint8_t hw_pseudoInterrupts(void)
{
    if(codigoInterrupt==CUENTA_VIGENTE){
        double diffTiempos=difftime(time(NULL), inicioCuenta)*5000;
        if(diffTiempos>ESPERA_VEHICULO_CIRCULANDO){
            codigoInterrupt=TOCA_CERRAR;
        }
    }
    return codigoInterrupt;
}

void hw_flushInterrupts(void)
{
    codigoInterrupt=0;
}

/*==================[end of file]============================================*/
