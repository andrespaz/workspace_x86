/*==================[inclusions]=============================================*/

#include "fsm_cafetera.h"
#include "hw.h" //¿Es correcto hacer esto? Es para tener las funciones de hardware en la màquina de estados...


void fsm_cafetera_runCycle(){

    switch (state){
        case REPOSO:
            if(evtIngresaMoneda){
                printf("\n\nSeleccione una bebida...");
                hw_setDestellador(DESTELLO_APAGADO);
                state=SELECCION_BEBIDA;
            }
            break;

        case SELECCION_BEBIDA:
            if(evTick100ms && (count_ms < TIEMPO_DEVOLUCION_MONEDA)){
                count_ms+=100;
            }
            else if(evTick100ms && (count_ms == TIEMPO_DEVOLUCION_MONEDA)){
                count_ms=0;
                hw_devolverMoneda();
                hw_setDestellador(DESTELLO_REPOSO);
                state=REPOSO;
            }

            if(!(evtSelectTe && evtSelectCafe)){        //Si no se ejecutan ambos eventos de manera simultánea...
                if(evtSelectTe){
                    count_ms=0;
                    bebida=TE;
                    tiempoBebida=TIEMPO_ENTREGA_TE;
                    hw_salidaBebidas(bebida, ACT_ABRIR);
                    hw_setDestellador(DESTELLO_SIRVIENDO_BEBIDA);
                    state=SIRVIENDO_BEBIDA;
                }
                else if (evtSelectCafe){
                    count_ms=0;
                    bebida=CAFE;
                    tiempoBebida=TIEMPO_ENTREGA_CAFE;
                    hw_salidaBebidas(bebida, ACT_ABRIR);
                    hw_setDestellador(DESTELLO_SIRVIENDO_BEBIDA);
                    state=SIRVIENDO_BEBIDA;
                }
            }

            break;

        case SIRVIENDO_BEBIDA:
            if(evTick100ms && (count_ms < tiempoBebida)){
                count_ms+=100;
            }
            else if(evTick100ms && (count_ms == tiempoBebida)){
                count_ms=0;
                hw_indicadorSonoro_toggle();
                hw_salidaBebidas(bebida, ACT_CERRAR);
                hw_setDestellador(DESTELLO_APAGADO);
                printf("Retire su bebida...");
                state=ENTREGA_LISTA;
            }
            break;

        case ENTREGA_LISTA:
            if(evTick100ms && (count_ms < TIEMPO_AVISO_BEBIDA_LISTA)){
                count_ms+=100;
            }
            else if(evTick100ms && (count_ms == TIEMPO_AVISO_BEBIDA_LISTA)){
                count_ms=0;
                hw_indicadorSonoro_toggle();
                hw_setDestellador(DESTELLO_REPOSO);
                state=REPOSO;
            }
            break;
    }
    fsm_clean_evts();
}



void fsm_cafetera_init(){
    hw_setDestellador(DESTELLO_REPOSO);
    state=REPOSO;
    fsm_clean_evts();
}

void fsm_clean_evts(){
    evtIngresaMoneda=false;
    evtSelectTe=false;
    evtSelectCafe=false;
    evTick100ms=false;
}


void fsm_cafetera_ingresaMoneda(){
    evtIngresaMoneda=true;
}

void fsm_cafetera_elijoBebida(uint8_t bebida){
    switch (bebida){
        case TE:
            evtSelectTe=true;
        break;
        case CAFE:
            evtSelectCafe=true;
        break;
    }
}

void fsm_cafetera_raise_evtTick100mseg(){
    evTick100ms=true;
}

