/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"
#include "fsm_cafetera.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;
    uint16_t cont_ms;
    uint16_t cont_100ms;

    fsm_cafetera_init();
    hw_Init();

    while (1) {
        input = hw_LeerEntrada();

        if (input == SENSOR_MONEDERO) {
            fsm_cafetera_ingresaMoneda();
        }

        if (input == TECLA_TE) {
            fsm_cafetera_elijoBebida(TE);
        }

        if (input == TECLA_CAFE) {
            fsm_cafetera_elijoBebida(CAFE);
        }

        cont_ms++;
        cont_100ms++;
        
        if(cont_100ms == 100){
            cont_100ms=0;
            fsm_cafetera_raise_evtTick100mseg();
        }
        hw_destelladorLed();
        fsm_cafetera_runCycle();
        hw_Pausems(1);
    }

    hw_DeInit();
    return 0;
}



/*==================[end of file]============================================*/
