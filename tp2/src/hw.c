/*==================[inclusions]=============================================*/

#include "fsm_cafetera.h" //Pregunta: ¿Es correcto poner esto acá? (Es sólo para reutilizar las macros y no tener que copiar y pegar)

#include "hw.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

#include <stdbool.h>
/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;


/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);       
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);    
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}

void hw_salidaBebidas(uint8_t bebida, uint8_t accion)
{
    if(bebida==TE){

        if(accion==ACT_ABRIR){
            printf("\n*Se abre la electrovalvula del te* \n\n");
        }
        else if (accion==ACT_CERRAR){
            printf("\n*Cerrando la electrovalvula del te* \n\n");
        }
    }
    else if (bebida==CAFE){
        if(accion==ACT_ABRIR){
            printf("\n*Se abre la electrovalvula del cafe* \n\n");
        }
        else if (accion==ACT_CERRAR){
            printf("\n*Cerrando la electrovalvula del cafe* \n\n");
        }
    }

}

void hw_indicadorSonoro_toggle()
{
    indicadorSonoro_on=!indicadorSonoro_on;
    if(indicadorSonoro_on){
        printf("\n\nBUZZER ON \n");
    }
    else{
        printf("\nBUZZER OFF \n");
    }
}

void hw_setDestellador(uint16_t nuevoTiempo){
    countDestellador_ms=0;
    tiempoDestello=nuevoTiempo; 
}

void hw_destelladorLed(){
 
    if(tiempoDestello==DESTELLO_APAGADO){
        return;
    }
    
    if(evTick100ms && (countDestellador_ms < tiempoDestello)){
        countDestellador_ms+=100;
    }
    else if(evTick100ms && (countDestellador_ms == tiempoDestello)){
        if(!led_on){
            printf("\nON_LED\n");
        }
        else{
            printf("\nOFF_LED\n");
        }
        countDestellador_ms=0;
        led_on=!led_on;
    }
}

void hw_devolverMoneda(void){
    printf("\nSe devuelve la moneda por no haber seleccionado nada \n\n");
}

/*==================[end of file]============================================*/
