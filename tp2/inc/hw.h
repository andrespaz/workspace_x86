#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>
#include <stdbool.h>


/*==================[macros]=================================================*/

#define EXIT      27  // ASCII para la tecla Esc 
#define SENSOR_MONEDERO  49  // ASCII para la tecla 1 
#define TECLA_TE  53  // ASCII para la tecla 5 
#define TECLA_CAFE  54  // ASCII para la tecla 6 


//Tiempos definidos en ms
#define DESTELLO_APAGADO 0
#define DESTELLO_REPOSO 500 
#define DESTELLO_SIRVIENDO_BEBIDA 100

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/
uint16_t countDestellador_ms;
uint16_t tiempoDestello;
bool led_on;
bool indicadorSonoro_on;

/*==================[external functions declaration]=========================*/

void hw_Init(void);
void hw_DeInit(void);
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);

void hw_salidaBebidas(uint8_t, uint8_t);
void hw_indicadorSonoro_toggle();
void hw_setDestellador(uint16_t);
void hw_destelladorLed();
void hw_devolverMoneda(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
