#ifndef _FSM_CAFETERA_H_
#define _FSM_CAFETERA_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>
#include <stdbool.h> //¿Pregunta: es buena práctica incluir una biblioteca sólo para usar variables booleanas... o conviene usar un uint como pseudobool?
#include <stdio.h>  
/*==================[macros]=================================================*/

//Estados posibles

#define REPOSO 1
#define SELECCION_BEBIDA 2
#define SIRVIENDO_BEBIDA 3
#define ENTREGA_LISTA 4

//Bebidas posibles

#define TE 60 
#define CAFE 61

//Códigos de acciones

#define ACT_ABRIR   20  
#define ACT_CERRAR  21  

//Tiempos definidos en ms
#define TIEMPO_AVISO_BEBIDA_LISTA 2000
#define TIEMPO_DEVOLUCION_MONEDA 30000          
#define TIEMPO_ENTREGA_TE 30000        
#define TIEMPO_ENTREGA_CAFE 45000   


/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

uint8_t state;
uint8_t bebida;
uint16_t tiempoBebida;
bool evtIngresaMoneda;
bool evtSelectTe;
bool evtSelectCafe;
bool evTick100ms;
uint16_t count_ms;

/*==================[external functions declaration]=========================*/

void fsm_cafetera_runCycle(void);

void fsm_cafetera_init(void);
void fsm_clean_evts(void);

void fsm_cafetera_ingresaMoneda(void);
void fsm_cafetera_elijoBebida(uint8_t);
void fsm_cafetera_raise_evtTick100mseg(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _FSM_CAFETERA_H_ */
